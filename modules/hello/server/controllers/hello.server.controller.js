'use strict';

/**
 * Module dependencies.
 */
var path = require('path'),
  mongoose = require('mongoose'),
  Hello = mongoose.model('Hello'),
  errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller')),
  _ = require('lodash');

/**
 * Create a Hello
 */
exports.create = function (req, res) {

};

/**
 * Show the current Hello
 */
exports.read = function (req, res) {

};

/**
 * Update a Hello
 */
exports.update = function (req, res) {

};

/**
 * Delete an Hello
 */
exports.delete = function (req, res) {

};

/**
 * List of Hellos
 */
exports.list = function (req, res) {
  Hello.find().sort('-created').exec(function (err, hellos) {
   if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(hellos);
    }
 });
};
