'use strict';

var hello = require('../controllers/hello.server.controller');

module.exports = function(app) {
  // Routing logic   
  // ...
  app.route('/api/hello').all()
    .get(hello.list);
};
