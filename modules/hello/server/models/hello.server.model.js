'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

/**
 * Hello Schema
 */
var HelloSchema = new Schema({
  // Hello model fields
  stime: {
    type: String,
    default: ''
  }
});

mongoose.model('Hello', HelloSchema);
