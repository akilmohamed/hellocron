'use strict';

/**
 * Module dependencies
 */
var path = require('path'),
  CronJob = require('cron').CronJob,
  mongoose = require('mongoose'),
  dateFormat = require('dateformat'),
  Hello = mongoose.model('Hello'),
  config = require(path.resolve('./config/config'));

/**
 * Hello module init function.
 */
module.exports = function (app, db) {

  //var job = new CronJob('* * * * *', function() { // 1minute
  var job = new CronJob('0 * * * *', function() { //1hour

    var now = new Date();
    var date = dateFormat(now, 'h:MMtt, dd mmm yyyy');
  
    // Create new Article object
    var hello = new Hello({
      stime: date
    }).save();

  }, function () {
	
  },
    true, /* Start the job right now */
    'Asia/Kuala_Lumpur' /* Time zone of this job. */
  );

};
