(function() {
  'use strict';

  angular
    .module('hello')
    .controller('HelloController', HelloController);

  HelloController.$inject = ['$scope', 'Hello'];

  function HelloController($scope, Hello) {
    var vm = this;

    $scope.find = function() {
      $scope.hellos = Hello.query();
      console.log(Hello.query());
    };


    init();

    function init() {
    }
  }
})();
