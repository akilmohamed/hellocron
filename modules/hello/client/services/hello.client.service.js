'use strict';

// Users service used for communicating with the users REST endpoint
angular.module('hello').factory('Hello', ['$resource',
  function ($resource) {
    return $resource('api/hello', {}, {
      update: {
        method: 'PUT'
      }
    });
  }
]);