(function () {
  'use strict';

  //Setting up route
  angular
    .module('hello')
    .config(routeConfig);

  routeConfig.$inject = ['$stateProvider'];

  function routeConfig($stateProvider) {
    // Hello state routing
    $stateProvider
      .state('hello', {
        url: '/hello',
        templateUrl: 'modules/hello/client/views/hello.client.view.html',
        controller: 'HelloController',
        controllerAs: 'vm'
      });
  }
})();
