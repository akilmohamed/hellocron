(function() {
  'use strict';

  // Hello module config
  angular
    .module('hello')
    .run(menuConfig);

  menuConfig.$inject = ['Menus'];

  function menuConfig(Menus) {
    // Config logic
    // ...
    Menus.addMenuItem('topbar', {
      title: 'Hello',
      state: 'hello',
      roles: ['*']
    });
  }
})();
